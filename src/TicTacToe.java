import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class TicTacToe implements  Runnable {

    //setting up variables
    Scanner sc;
     String ip;
     int port;
     JFrame jframe;
    int width,height;
    Thread thread;
    Painter painter;
    Socket socket;
    DataOutputStream dos;
    DataInputStream dis;
    ServerSocket serverSocket;
    BufferedImage board ,redX , blueX , redO , blueO;
    String [] spaces;
    boolean yourTurn,circle,acc,unableToCon,won,enemyWon,tie;
    int len , errors,firstSpot,secondSpot;
    Font font , smallerFont, largerFont;
    String waitingNotif,unableNotif , winNotif , enemyWonNotif , tieNotif;
    int [][] winsCondition;

    public TicTacToe(){
        this.sc = new Scanner(System.in);
        this.width = 506;
        this.height =527;
        this.spaces = new String[9];
        this.yourTurn = false;
        this.circle = false;
        this.acc = false;
        this.unableToCon = false;
        this.won = false;
        this.enemyWon= false;
        this.tie= false;
        this.len = 160;
        this.errors = 0;
        this.firstSpot = -1;
        this.secondSpot = -1;
        this.font = new Font("Verdana",1,32);
        this.smallerFont = new Font("Verdana",1,20);
        this.largerFont = new Font("Verdana",1,50);
        this.waitingNotif = "waiting for opponent";
        this.unableNotif = "unable to comunicate with opponent";
        this.winNotif = "you won!";
        this.enemyWonNotif = "opponent won!";
        this.tieNotif = "tie";
        this.winsCondition = new int[][] {{0, 1, 2}, {3, 4, 5}, {6, 7, 8}, {0, 3, 6}, {1, 4, 7}, {2, 5, 8}, {0, 4, 8}, {2, 4, 6}};
        System.out.println("enter your ip addres ");
        this.ip = sc.nextLine();
        System.out.println("please enter the port");
        for(this.port = sc.nextInt(); this.port<1 || this.port>6535 ; this.port = sc.nextInt()){
            System.out.println("The port you entered is invalid (maybe  have been reserved please enter another port");
        }
        this.loadImages();
        this.painter = new TicTacToe.Painter();

        this.painter.setPreferredSize(new Dimension(506,527));
        if(!this.connect()){
            //if there is not server then we become host
            this.initializeServer();
        }

        this.jframe = new JFrame();
        this.jframe.setTitle("tic-tac-toe");
        this.jframe.setContentPane(this.painter);
        this.jframe.setSize(506,527);
        this.jframe.setLocationRelativeTo((Component) null);
        this.jframe.setDefaultCloseOperation(3);
        this.jframe.setVisible(true);
        this.thread = new Thread(this,"TicTacToe");
        this.thread.start();






    }


    @SuppressWarnings("unused")
    public static void main(String[] args) {
        TicTacToe ticTacToe = new TicTacToe();
    }
//    Connect Session here
   private boolean connect() {
    try {
        this.socket = new Socket(this.ip, this.port);
        this.dos = new DataOutputStream(this.socket.getOutputStream());
        this.dis = new DataInputStream(this.socket.getInputStream());
        this.acc = true;
    } catch (IOException var2) {
        System.out.println("Unable to connect to the address: " + this.ip + ":" + this.port + " | Starting a server");
        return false;
    }

    System.out.println("Successfully connected to the server.");
    return true;
}

    private void initializeServer() {
        try {
            this.serverSocket = new ServerSocket(this.port, 8, InetAddress.getByName(this.ip));
        } catch (Exception var2) {
            var2.printStackTrace();
        }

        this.yourTurn = true;
        this.circle = false;
    }

    private void listenForServerRequest() {
        Socket socket = null;

        try {
            socket = this.serverSocket.accept();
            this.dos = new DataOutputStream(socket.getOutputStream());
            this.dis = new DataInputStream(socket.getInputStream());
            this.acc = true;
            System.out.println("CLIENT HAS REQUESTED TO JOIN, AND WE HAVE ACCEPTED");
        } catch (IOException var3) {
            var3.printStackTrace();
        }

    }


//    end of connect Seasion

    private void loadImages() {
        try {
            this.board = ImageIO.read(this.getClass().getResourceAsStream("res/board.png"));
            this.redX = ImageIO.read(this.getClass().getResourceAsStream("res/redX.png"));
            this.redO = ImageIO.read(this.getClass().getResourceAsStream("res/redCircle.png"));
            this.blueX = ImageIO.read(this.getClass().getResourceAsStream("res/blueX.png"));
            this.blueO = ImageIO.read(this.getClass().getResourceAsStream("res/blueCircle.png"));
        } catch (IOException var2) {
            var2.printStackTrace();
        }

    }

    private void render(Graphics g) {
        g.drawImage(this.board, 0, 0, (ImageObserver)null);
        int stringWidth;
        Graphics2D g2;
        if (this.unableToCon) {
            g.setColor(Color.RED);
            g.setFont(this.smallerFont);
            g2 = (Graphics2D)g;
            g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            stringWidth = g2.getFontMetrics().stringWidth(this.unableNotif);
            g.drawString(this.unableNotif, 253 - stringWidth / 2, 263);
        } else {
            if (this.acc) {
                for(int i = 0; i < this.spaces.length; ++i) {
                    if (this.spaces[i] != null) {
                        if (this.spaces[i].equals("X")) {
                            if (this.circle) {
                                g.drawImage(this.redX, i % 3 * this.len + 10 * (i % 3), i / 3 * this.len + 10 * (i / 3), (ImageObserver)null);
                            } else {
                                g.drawImage(this.blueX, i % 3 * this.len + 10 * (i % 3), i / 3 * this.len + 10 * (i / 3), (ImageObserver)null);
                            }
                        } else if (this.spaces[i].equals("O")) {
                            if (this.circle) {
                                g.drawImage(this.blueO, i % 3 * this.len + 10 * (i % 3), i / 3 * this.len + 10 * (i / 3), (ImageObserver)null);
                            } else {
                                g.drawImage(this.redO, i % 3 * this.len + 10 * (i % 3), i / 3 * this.len + 10 * (i / 3), (ImageObserver)null);
                            }
                        }
                    }
                }

                if (this.won || this.enemyWon) {
                    g2 = (Graphics2D)g;
                    g2.setStroke(new BasicStroke(10.0F));
                    g.setColor(Color.BLACK);
                    g.drawLine(this.firstSpot % 3 * this.len + 10 * this.firstSpot % 3 + this.len/ 2, this.firstSpot / 3 * this.len + 10 * (this.firstSpot / 3) + this.len / 2, this.secondSpot % 3 * this.len + 10 * this.secondSpot % 3 + this.len / 2, this.secondSpot / 3 * this.len + 10 * (this.secondSpot / 3) + this.len / 2);
                    g.setColor(Color.RED);
                    g.setFont(this.largerFont);
                    if (this.won) {
                        stringWidth = g2.getFontMetrics().stringWidth(this.winNotif);
                        g.drawString(this.winNotif, 253 - stringWidth / 2, 263);
                    } else if (this.enemyWon) {
                        stringWidth = g2.getFontMetrics().stringWidth(this.enemyWonNotif);
                        g.drawString(this.enemyWonNotif, 253 - stringWidth / 2, 263);
                    }
                }

                if (this.tie) {
                    g2 = (Graphics2D)g;
                    g.setColor(Color.BLACK);
                    g.setFont(this.largerFont);
                    stringWidth = g2.getFontMetrics().stringWidth(this.tieNotif);
                    g.drawString(this.tieNotif, 253 - stringWidth / 2, 263);
                }
            } else {
                g.setColor(Color.RED);
                g.setFont(this.font);
                g2 = (Graphics2D)g;
                g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                stringWidth = g2.getFontMetrics().stringWidth(this.waitingNotif);
                g.drawString(this.waitingNotif, 253 - stringWidth / 2, 263);
            }

        }
    }


    private void checkForWin() {
        for(int i = 0; i < this.winsCondition.length; ++i) {
            if (this.circle) {
                if (this.spaces[this.winsCondition[i][0]] == "O" && this.spaces[this.winsCondition[i][1]] == "O" && this.spaces[this.winsCondition[i][2]] == "O") {
                    this.firstSpot = this.winsCondition[i][0];
                    this.secondSpot = this.winsCondition[i][2];
                    this.won = true;
                }
            } else if (this.spaces[this.winsCondition[i][0]] == "X" && this.spaces[this.winsCondition[i][1]] == "X" && this.spaces[this.winsCondition[i][2]] == "X") {
                this.firstSpot = this.winsCondition[i][0];
                this.secondSpot = this.winsCondition[i][2];
                this.won = true;
            }
        }

    }


    private void checkForTie() {
        for(int i = 0; i < this.spaces.length; ++i) {
            if (this.spaces[i] == null) {
                return;
            }
        }

        this.tie = true;
    }



    private void checkForEnemyWin() {
        for(int i = 0; i < this.winsCondition.length; ++i) {
            if (this.circle) {
                if (this.spaces[this.winsCondition[i][0]] == "X" && this.spaces[this.winsCondition[i][1]] == "X" && this.spaces[this.winsCondition[i][2]] == "X") {
                    this.firstSpot = this.winsCondition[i][0];
                    this.secondSpot = this.winsCondition[i][2];
                    this.enemyWon = true;
                }
            } else if (this.spaces[this.winsCondition[i][0]] == "O" && this.spaces[this.winsCondition[i][1]] == "O" && this.spaces[this.winsCondition[i][2]] == "O") {
                this.firstSpot = this.winsCondition[i][0];
                this.secondSpot = this.winsCondition[i][2];
                this.enemyWon = true;
            }
        }

    }
    private void tick() {
        if (this.errors >= 10) {
            this.unableToCon = true;
        }

        if (!this.yourTurn && !this.unableToCon) {
            try {
                int space = this.dis.readInt();
                if (this.circle) {
                    this.spaces[space] = "X";
                } else {
                    this.spaces[space] = "O";
                }

                this.checkForEnemyWin();
                this.checkForTie();
                this.yourTurn = true;
            } catch (IOException var2) {
                var2.printStackTrace();
                ++this.errors;
            }
        }

    }






    @Override
    public void run() {
        while (true) {
            tick();
            painter.repaint();

            if (!this.circle && !this.acc) {
                listenForServerRequest();
            }

        }
    }

    private class Painter extends JPanel implements MouseListener {

        public Painter(){
            this.setFocusable(true);
            this.requestFocus();
            this.setBackground(Color.white);
            this.addMouseListener(this);
        }

        public void paintComponent(Graphics g){
            super.paintComponent(g);
            TicTacToe.this.render(g);

        }

        @Override
        public void mouseClicked(MouseEvent e) {
            if (TicTacToe.this.acc && TicTacToe.this.yourTurn && !TicTacToe.this.unableToCon && !TicTacToe.this.won && !TicTacToe.this.enemyWon) {
                int x = e.getX() / TicTacToe.this.len;
                int y = e.getY() / TicTacToe.this.len;
                y *= 3;
                int position = x + y;
                if (TicTacToe.this.spaces[position] == null) {
                    if (!TicTacToe.this.circle) {
                        TicTacToe.this.spaces[position] = "X";
                    } else {
                        TicTacToe.this.spaces[position] = "O";
                    }

                    TicTacToe.this.yourTurn = false;
                    this.repaint();
                    Toolkit.getDefaultToolkit().sync();

                    try {
                        TicTacToe.this.dos.writeInt(position);
                        TicTacToe.this.dos.flush();
                    } catch (IOException var6) {
                        TicTacToe.this.errors = TicTacToe.this.errors + 1;
                        var6.printStackTrace();
                    }

                    System.out.println("DATA WAS SENT");
                    TicTacToe.this.checkForWin();
                    TicTacToe.this.checkForTie();
                }
            }

        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }
    }
}








